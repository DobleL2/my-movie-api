import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker # Permite crear la sesion
from sqlalchemy.ext.declarative import declarative_base # Nos permite manipular todas las tablas de nuestra base de datos

sqlite_file_name = "../database.sqlite"

base_dir = os.path.dirname(os.path.realpath(__file__)) # Leer el directorio actual de este archivo

database_url = f"sqlite:///{os.path.join(base_dir,sqlite_file_name)}" # Iniciar una base de datos sqlite  con url

engine = create_engine(database_url, echo=True)   # Representa el motor de la base de datos

Session = sessionmaker(bind=engine)# Crear una sesión para conectarse a la base de datos

Base = declarative_base() # Creación de una instancia